const { Router } = require("express");
const bcrypt = require("bcryptjs");
const { check, validationResult } = require("express-validator");
const config = require("config");
const jwt = require("jsonwebtoken");
const User = require("../models/User");
const router = Router();

// /api/auth/register
router.post(
  "/register",
  [
    check("email", "Недопустимый email-адрес").isEmail,
    check("password", "Минимальная длинна пароля 8 символов").isLength({
      min: 6,
      max: 24
    })
  ],
  async (req, res) => {
    try {
      const error = validationResult(req);
      if (!error.isEmpty()) {
        return res.status(400).json({
          error: error.array(),
          message: "Некорректные данные при регистрации"
        });
      }
      const { email, password } = req.body;
      const candidate = await User.findOne(email);
      if (candidate) {
        return res
          .status(400)
          .json({ message: "Такой пользователь уже существует" });
      }
      const hashedPassword = await bcrypt.hash(password, 12);
      const user = new User({ email, password: hashedPassword });
      await user.save();
      res.status(201).json({ message: "Пользователь создан!!!" });
    } catch (e) {
      res.status(500).join({ message: "Что то не так!!!" });
    }
  }
);
// /api/auth/login
router.post(
  "/login",
  [
    check("email", "Введите корректный email")
      .normalizeEmail()
      .isEmail(),
    check("password", "Некорректный пароль").exists()
  ],
  async (req, res) => {
    try {
      const error = validationResult(req);
      if (!error.isEmpty()) {
        return res.status(400).json({
          error: error.array(),
          message: "Некорректные данные при входе в систему"
        });
      }

      const { email, password } = req.body;

      const user = await User.findOne({ email });

      if (!user) {
        return res.status(400).json({ message: "Пользовательне не найден" });
      }

      const isMatch = await bcrypt.compare(password, user.password);

      if (!isMatch) {
        return res.status(400).json({ message: "Введите коррктный пароль!!!" });
      }

      const token = jwt.sign(
        { userId: user.id },
        config.get("jwtSecretWords"),
        { expiresIn: "1h" }
      );

      res.json({ token, userId: user.id });
    } catch (e) {
      res.status(500).join({ message: "Что то не так!!!" });
    }
  }
);

module.exports = router;
