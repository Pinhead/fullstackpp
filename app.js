const express = require("express")
const app = express()
const config = require("config")
const mongoose = require("mongoose")

const PORT = config.get("port") || 3000

app.use("/api/auth", require("./routes/auth.routes"))

async function start() {
  try {
    mongoose.connect(config.get("mongoUrl"), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    })
    app.listen(PORT, () => {
      console.log("Port: ", PORT)
    })
    0
  } catch (e) {
    console.log("Server Error", e.message)

    process.exit(1)
  }
}
start()
